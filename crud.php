<?php

$db = mysqli_connect("localhost", "", "", "");
$db->query("set names utf8");

$edit_id = (int)$_REQUEST['id'] ?? 0;
$del_id  = (int)($_GET['del'] ?? 0);

$in = ['name' => '', 'status' => ''];

if ($del_id) {
    $db->query("DELETE FROM test1 WHERE id = $del_id");
}

// Редактирование
if ($_SERVER['REQUEST_METHOD'] == 'POST') {

    $_name   = addslashes($_POST['name']);
    $_status = addslashes($_POST['status']);

    if ($_status == '') {
        echo '<script>alert("Выберите статус!!!") </script>';
        echo '<script>window.location.href = "crud.php"</script>';
        exit;
    }

    if ($_name == '') {
        echo '<script>alert("Выберите имя!!!") </script>';
        echo '<script>window.location.href = "crud.php"</script>';
        exit;
    }

    if ($edit_id) {
        $db->query("UPDATE test1 SET `name` = '$_name', status = '$_status' WHERE id = $edit_id");
    } else {
        $db->query("INSERT INTO test1 (`name`, `status`) VALUES ('$_name', '$_status')");
    }

    header('Location: crud.php');
    exit;
}

// Получение новых данных для редактирования, если есть
if ($edit_id) {
    $in = $db->query("SELECT * FROM test1 WHERE id = $edit_id")->fetch_assoc();
    $in['status'] = mb_strtolower($in['status']);
}

// =============================== ОТОБРАЖЕНИЕ

echo '<title>CRUD</title>';
echo '<meta charset="utf8">';

echo '<h3>Демонстрация добавления/редактирования/удаления строк</h3>';

echo '<table border="1" cellpadding="4" cellspacing="0">';
echo '<tr><th>ID</th><th>NAME</th><th>STATUS</th><th></th></tr>';
foreach ($db->query("SELECT * FROM test1 LIMIT 250") as $row) {

    echo '<tr>';
    echo '<td><a href="crud.php?id='.$row['id'].'">'.$row['id'].'</a></td>';
    echo '<td>'.$row['name'].'</td>';
    echo '<td>'.$row['status'].'</td>';
    echo '<td><a href="crud.php?del='.$row['id'].'">Удалить</a></td>';
    echo '</tr>';
}
echo '</table>';

?>
<br>
<style>p { padding: 4px 0; margin: 0; }</style>
<form action="crud.php" method="post">
    <input type="hidden" name="id" value="<?=$edit_id;?>">
    <p><input required type="text" name="name" value="<?=$in['name'] ?? '';?>"> Имя</p>
    <p>
        <select style="width: 177px;" name="status">
            <option selected disabled>-Выберите статус-</option>
            <option <?php if ($in['status'] == 'ok') { echo 'selected'; } ?> value="ok">ОК</option>
            <option <?php if ($in['status'] == 'approve') { echo 'selected'; } ?> value="approve">Approve</option>
            <option <?php if ($in['status'] == 'disapprove') { echo 'selected'; } ?> value="disapprove">Disapprove</option>
        </select>
    <p><button><?=$edit_id ? 'Редактировать' : 'Добавить'; ?></button></p>
</form>
