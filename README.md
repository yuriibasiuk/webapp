# About Webapp

## Step by step solution
### 0. Installed Ubuntu 22.04.1 LTS (GNU/Linux 5.15.0-52-generic x86_64)
### 1. Installed web server `Apache2`
```shell
sudo apt install apache2
```
### 2. Installed `MariaDB`
```shell
sudo apt install mariadb-server
sudo mysql_secure_installation
```
### 3. Created a database `TEST1`
```shell
CREATE DATABASE `test1` CHARACTER SET utf8 COLLATE utf8_general_ci;
CREATE USER 'testuser'@'localhost' IDENTIFIED BY '';
GRANT ALL PRIVILEGES ON test1.* TO 'testuser'@'localhost' IDENTIFIED BY '';
```
### 4. Created a table and 3 fields (`id`, `name`,`status`)
```shell
CREATE TABLE `test1` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(200) NOT NULL DEFAULT '',
  `status` varchar(50) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=8 DEFAULT CHARSET=utf8
```

### 5. Created a web page `crud.php` for create, read, update and delete data
- https://gitlab.com/yuriibasiuk/webapp/-/blob/main/crud.php

### 6. Created a web page `index.php` for view data with filter
- https://gitlab.com/yuriibasiuk/webapp/-/blob/main/index.php

### Other
- installed SSL certificate
- shared the web app
