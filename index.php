<?php
$db = mysqli_connect("localhost", "", "", "");
$db->query("set names utf8");

$q_name   = $_GET['name'] ?? "";
$q_status = $_GET['status'] ?? "";

$statuses = [
    'ok' => "OK",
    'approve' => "Approve",
    'disapprove' => "Disapprove",
];

?>

<title>View data</title>
<meta charset="utf8">
<h3>Демонстрация выборки строк</h3>

<form action="?" method="get">
    NAME <input type="text" name="name" value="<?=$q_name;?>">
    STATUS
    <select name="status">
        <option value="">--</option>
        <?php foreach ($statuses as $a => $b): ?>
            <option value="<?=$a;?>" <?=$q_status == $a ? 'selected' : ''; ?>><?=$b;?></option>
        <?php endforeach; ?>
    </select>
    <button>Фильтр</button>
</form>

<?php

$where = "1";

if ($q_name) $where .= " AND name LIKE '%".addslashes($q_name)."%' ";
if ($q_status) $where .= " AND status = '".addslashes($q_status)."' ";

$sql = "SELECT * FROM test1 WHERE $where LIMIT 250";

echo '<table border="1" cellpadding="4" cellspacing="0">';
echo '<tr><th>ID</th><th>NAME</th><th>STATUS</th></tr>';
foreach ($db->query($sql) as $row) {

    echo '<tr>';
    echo '<td>'.$row['id'].'</td>';
    echo '<td>'.$row['name'].'</td>';
    echo '<td>'.$row['status'].'</td>';
    echo '</tr>';
}
echo '</table>';
